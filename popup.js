var settings = {
    subfolders: {}
};

function save_options() {
    localStorage['settings'] = JSON.stringify(settings);
}

function load_settings() {
    if (localStorage['settings']) {
        settings = $.parseJSON(localStorage['settings'])
        if (!settings.subfolders) {
            settings.subfolders = {}
        }
    }
}

// Update the relevant fields with the new data
function renderView(typo3cache) {
    if (localStorage['settings']) {
        settings = $.parseJSON(localStorage['settings']);
    }
    if (typo3cache) {
        settings.subfolder = false;
        if (settings.subfolders[typo3cache.host]) {
            settings.subfolder = settings.subfolders[typo3cache.host];
        }
    }

    var status;
    if (!typo3cache || !typo3cache.hasTypo3) {
        status = 404;
        typo3cache = typo3cache || {hasTypo3: false};
    } else {
        status = typo3cache.status;
    }

    var template = Handlebars.compile($('#template-' + status).html());
    var rendered = template({
        typo3cache: typo3cache,
        settings: settings,
        subfolder: settings.subfolder
    });
    $('#typo3_clearcache').html(rendered);
    if (status == 200) {
        chrome.tabs.getSelected(null, function(tab) {
            localStorage['_cache_' + tab.id] = JSON.stringify({
                content: rendered,
                time: new Date().getTime()
            });
        });
    }
    registerEvents(typo3cache);
}

// Register events for rendered and placed elements
var registerEvents = function(typo3cache) {
    $('a').not('.approved').not('.action').click(function(event){
        event.preventDefault();
        chrome.runtime.sendMessage({
            from: 'popup',
            subject: 'clearcache',
            url: $(this).attr('href')
        });
        window.close();
    });

    $('a.action').click(function() {
        $(this).hide();
        $(this).next('div.action').show().find('input:first').focus();
    });

    if (typo3cache) {
        // Add subdirectory for current host
        $('div.action form').submit(function(event) {
            event.preventDefault();
            var value = this.subfolder.value;
            if (value.substr(0, 1) === '/') {
                value = value.substr(1);
            }
            settings.subfolders[typo3cache.host] = value;
            if (value === '') {
                delete settings.subfolders[typo3cache.host];
            }
            save_options();
            window.close();
        });
    }

    $('lazyimg').each(function(){
        $('<img>')
            .attr('src', $(this).attr('src'))
            .insertAfter($(this))
        ;
        $(this).remove();
    });
};

// Once the DOM is ready...
window.addEventListener('DOMContentLoaded', function () {
    load_settings();

    chrome.tabs.getSelected(null, function(tab) {
        var localCache = localStorage['_cache_' + tab.id];
        if (localCache) {
            var local = $.parseJSON(localCache);
            // Cache just one minute valid
            var now = new Date().getTime();
            if (now - local.time > 60000) {
                delete localStorage['_cache'];
            } else {
                $('#typo3_clearcache').html(local.content);
                $('a').click(function(){$(this).css('background', 'red');});
                registerEvents();
            }
        }
    });

    chrome.tabs.getSelected(null, function(tab) {
        chrome.tabs.sendMessage(
            tab.id,
            {from: 'popup', subject: 'DOMInfo', settings: settings},
            renderView
        );
    });
});
