var settings = {
	subfolders: {},
	reloadPageAfterCacheClear: true
};

function save_settings() {
	localStorage['settings'] = JSON.stringify(settings);
}

function load_settings() {
	if (localStorage['settings']) {
		settings = $.parseJSON(localStorage['settings'])
		if (!settings.subfolders) {
			settings.subfolders = {}
		}
	}
}

function renderView() {
	var template = Handlebars.compile($('#template-options').html());
	var rendered = template({
		settings: settings,
		version: chrome.runtime.getManifest().version
	});
	$('#typo3_clearcache_options').html(rendered);
}

function restore_options()
{
	// Restore IDE Key
	idekey = localStorage["xdebugIdeKey"];

	if (!idekey)
	{
		idekey = "XDEBUG_ECLIPSE";
	}

	if (idekey == "XDEBUG_ECLIPSE" || idekey == "netbeans-xdebug" || idekey == "macgdbp" || idekey == "PHPSTORM")
	{
		$("#ide").val(idekey);
		$("#idekey").prop('disabled', true);
	}
	else
	{
		$("#ide").val("null");
		$("#idekey").prop('disabled', false);
	}
	$('#idekey').val(idekey);

	// Restore Trace Triggers
	var traceTrigger = localStorage["xdebugTraceTrigger"];
	if (traceTrigger !== null)	{
		$("#tracetrigger").val(traceTrigger);
	} else {
		$("#tracetrigger").val(null);
	}

	// Restore Profile Triggers
	var profileTrigger = localStorage["xdebugProfileTrigger"];
	if (profileTrigger !== null)	{
		$("#profiletrigger").val(profileTrigger);
	} else {
		$("#profiletrigger").val(null);
	}
}

$(function()
{
	$("#ide").change(function ()
	{
		if ($("#ide").val() != "null")
		{
			$("#idekey").prop('disabled', true);
			$("#idekey").val($("#ide").val());

			save_settings();
		}
		else
		{
			$("#idekey").prop('disabled', false);
		}
	});

	$("#idekey").change(save_settings);


	// restore_options();

	load_settings();
	renderView();

	$('.save-button').click(save_settings);

	$('#reloadPageAfterCacheClear').change(function(){settings.reloadPageAfterCacheClear = this.value; });

});
